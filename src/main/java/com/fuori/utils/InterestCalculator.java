package com.fuori.utils;

import java.math.BigDecimal;

public class InterestCalculator {

    public static final double RATE_DEFAULT = 0;
    public static final double RATE_S = 1.0;
    public static final double RATE_M = 2.0;
    public static final double RATE_L = 3.0;

    public static BigDecimal calculateInterest(final BigDecimal rate, final BigDecimal balance) {
      return (rate.divide(BigDecimal.valueOf(100))).multiply(balance).setScale(2, BigDecimal.ROUND_HALF_EVEN);

    }

    public static BigDecimal calculateInterestForBalance(final BigDecimal balance) {
        return calculateInterest(calculateRateForBalance(balance), balance);
    }

    public static BigDecimal calculateRateForBalance(final BigDecimal balance) {

        double rate = RATE_DEFAULT;

        // >0 <1000 = 1%, >= 1000 < 5000 = 2%, >=5000 = 3%
        if (balance.compareTo(BigDecimal.valueOf(0)) == 1 && balance.compareTo(BigDecimal.valueOf(1000)) == -1) {

            rate = RATE_S;

        } else if ((balance.compareTo(BigDecimal.valueOf(1000)) == 1 || balance.compareTo(BigDecimal.valueOf(1000)) == 0) && balance.compareTo(BigDecimal.valueOf(5000)) == -1) {

            rate = RATE_M;

        } else if ((balance.compareTo(BigDecimal.valueOf(5000)) == 1 || balance.compareTo(BigDecimal.valueOf(5000)) == 0)) {

            rate = RATE_L;

        }

        return BigDecimal.valueOf(rate);
    }
}
