package com.fuori.utils;

import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: gavinastur
 * Date: 24/09/2012
 * Time: 07:20
 */
public class InterestCalculatorTest {

    @Test
    public void testGetInterest10Percent() {

        BigDecimal amount = BigDecimal.valueOf(250);
        BigDecimal rate = BigDecimal.valueOf(10);

        Assert.assertEquals("Interest not as expected: ", new BigDecimal("25.00"), InterestCalculator.calculateInterest(rate, amount));

    }

    @Test
    public void testGetInterest20Percent() {

        BigDecimal amount = BigDecimal.valueOf(50);
        BigDecimal rate = BigDecimal.valueOf(20);

        Assert.assertEquals("Interest not as expected: ", new BigDecimal("10.00"), InterestCalculator.calculateInterest(rate, amount));

    }

    @Test
    public void testGetInteresthalfPercent() {

        BigDecimal amount = new BigDecimal("100.0");
        BigDecimal rate = BigDecimal.valueOf(1.5);

        Assert.assertEquals("Interest not as expected: ", new BigDecimal("1.50"), InterestCalculator.calculateInterest(rate, amount));

    }

    @Test
    public void testgetVariableRateforAmountNegative(){

        BigDecimal balance = BigDecimal.valueOf(-1);

        Assert.assertEquals("Rate should be zero for negative balance", BigDecimal.valueOf(0.0), InterestCalculator.calculateRateForBalance(balance));
    }

    @Test
    public void testgetVariableRateforAmountZero(){

        BigDecimal balance = BigDecimal.valueOf(0);

        Assert.assertEquals("Rate should be zero for zero balance", BigDecimal.valueOf(0.0), InterestCalculator.calculateRateForBalance(balance));
    }

    @Test
    public void testgetVariableRateforAmount1to999(){

        BigDecimal balance = BigDecimal.valueOf(0.1);

        Assert.assertEquals("Rate should be 2.0% for balance = 1", BigDecimal.valueOf(1.0), InterestCalculator.calculateRateForBalance(balance));

        BigDecimal balance1 = BigDecimal.valueOf(999.99);

        Assert.assertEquals("Rate should be 1.0% for balance = 999", BigDecimal.valueOf(1.0), InterestCalculator.calculateRateForBalance(balance1));
    }

    @Test
    public void testgetVariableRateforAmount1000to4999(){

        BigDecimal balance = BigDecimal.valueOf(1000);

        Assert.assertEquals("Rate should be 2.0% for balance = 1000", BigDecimal.valueOf(2.0), InterestCalculator.calculateRateForBalance(balance));

        BigDecimal balance1 = BigDecimal.valueOf(4999);

        Assert.assertEquals("Rate should be 2.0% for balance = 4999", BigDecimal.valueOf(2.0), InterestCalculator.calculateRateForBalance(balance1));
    }

    @Test
    public void testgetVariableRateforAmount5000(){

        BigDecimal balance = BigDecimal.valueOf(5000);

        Assert.assertEquals("Rate should be 3.0% for balance = 5000", BigDecimal.valueOf(3.0), InterestCalculator.calculateRateForBalance(balance));


    }

    @Test
    public void testGetInterestFor1to999() {

        BigDecimal balance = BigDecimal.valueOf(0.1);

        Assert.assertEquals("Interest not as expected: ", new BigDecimal("0.00"), InterestCalculator.calculateInterestForBalance(balance));

        BigDecimal balance1 = BigDecimal.valueOf(999);

        Assert.assertEquals("Interest not as expected: ", BigDecimal.valueOf(9.99), InterestCalculator.calculateInterestForBalance(balance1));

    }

    @Test
    public void testGetInterestFor1000to4999() {

        BigDecimal balance = BigDecimal.valueOf(1000);

        Assert.assertEquals("Interest not as expected: ", new BigDecimal("20.00"), InterestCalculator.calculateInterestForBalance(balance));

        BigDecimal balance1 = BigDecimal.valueOf(4999);

        Assert.assertEquals("Interest not as expected: ", BigDecimal.valueOf(99.98), InterestCalculator.calculateInterestForBalance(balance1));

    }

    @Test
    public void testGetInterestFor15000() {

        BigDecimal balance = BigDecimal.valueOf(5000);

        Assert.assertEquals("Interest not as expected: ", new BigDecimal("150.00"), InterestCalculator.calculateInterestForBalance(balance));

    }

}
